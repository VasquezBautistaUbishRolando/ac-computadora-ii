# Computadora II
Este trabajo contiene los tres mapas conceptuales solicitados para la actividad "Computadora II", ambos son basados en los videos dados en la plataforma de Moodle.

## Computadoras Electrónicas
El siguiente mapa mental contiene una recopilación de las primeras computadoras electrónicas, así como, una historia de los componenetes que fueron creados para su uso posterior en las mismas.
```plantuml
@startmindmap
<style>
    arrow{
        LineColor blue
    }
</style>
    *[#Orange] Computadoras Electrónicas
        **[#lightgreen] Historia
            *** 1904
                **** John Ambrose Fleming
                    ***** Desarrolló la válvulo termoiónica.
                    ***** Compuesto de un filamento y 2 electrodos situados de un bulbo de cristal sellado.
                    ***** Cuando el filamento se calienta, permite el flujo de la corriente en una sola dirección.
            *** 1906 
                **** Lee De Forest
                    ***** Encontró el electrodo de control, este estaba situado entre el ánodo y el cátodo del diseño de Flemming.\nAl aplicar una carga positiva o negativa en el electro permite el flujo o detienen la corriente respectivamente.
                        ****** Logrando la función de los relés, pero sin el problema de las partes móviles,\ndando mas confiabilidad, mas durabilidad y mayor velocidad.
                        ****** Tenían la capacidad de cambiar de estado miles veces por segundo. Pero eran frágiles y se podían \nquemar como los focos, aparte de ser costosos, y las computadoras podrían requerir cientos de miles.
            *** 1947
                **** En los laboratorios Bell, John Bardeen, Walter \nBrattain, William Shockley, crearon el transistor.
                    ***** Con la creación de este, se inicio una nueva era para la computación.
                        ****** Un transistor tiene la misma funcionalidad que un relé o tubo \ndel vacío. Están hechos de silicio que paso por un “doping”.
                            ******* El “doping” se le agregan otros elementos al silicio que facilita la \nconducción de la electricidad. Dependiendo de los elementos que se \nle agreguen al silicio puede tener exceso o defectos de electrones.
                        ****** El transistor esta creado con 3 capas de este tipo de silicio en forma de sándwich.
                        ****** Su estructura esta conformada por un colector, base y un emisor.
                            ******* Podía tener 10 mil cambios por segundo, o 10 mil hz.
                        ****** Los transistores eran sólidos y podían hacerse mas pequeños que los relés y los tubos del vacío.
        **[#lightgreen] Harvard Mark I
            *** Construida durante la segunda guerra mundial para los aliados.
            *** Tenia 765,000 componentes, 80 km de cable, eje de 15 metros que atravesaba toda la estructura\npara manteneren sincronía el dispositivo, y utilizaba un motor de 5 caballos de fuerza.
            *** Uno de los usos que tuvo fue para hacer simulaciones y cálculos para el Proyecto Manhattan,\nproyecto que desarrollo la bomba atómica.
            *** La piedra angular fue el Relé: interruptor mecánico controlado eléctricamente.
                **** Conformado de una bobina, un brazo de hierro y 2 contactos.
                **** Cuando una corriente atraviesa la bobina se genera un campo magnético,\neste atrae el brazo metálico y cual une los contactos cerrando el circuito. 
            *** La Mark I contaba con 3500 relés. \nEsto hacia que fallará 1 relé al día.
            *** Al ser máquinas grandes y oscuras que se calentaban mucho atraía bichos. 
                **** En septiembre de 1947, gracias a Grace Hooper, los operadores de la Mark I sacaron una polilla de un relé,\npor eso, cada que fallaba algo en la Mark, se decía que tenía bichos (bugs en inglés).
            *** Se estaba buscando algo más confiable para reemplazar a los relés.
                **** En la década de los 40’s se hicieron mas accesibles y se hicieron factibles computadoras\n en base a estos componentes, al menos para la gente con bastante dinero.
                    ***** El cambio de relés a tubos del vacío supuso el hito que dejo atrás a las computadoras \nelectromecánicas y trajo la era de las computadoras electrónicas.
        **[#lightgreen] Colossus Mark I
            *** El primero uso de los tubos del vació.
            *** Desarrollada por Tommy Flowers terminada en diciembre de 1943,\ninstalada en Bletchley Park en Reino Unido.
                **** Servía para decodificar las comunicaciones nazis.
            *** Tenia 1600 tubos de vacío, y en total se construyeron 10 de estas \npara ayudar en la codificación.
            *** Considerada la primera computadora electrónica programable.
                **** La programación se realizaba mediante la conexión de cientos de cables.
        **[#lightgreen] ENIAC
            *** Por sus siglas, Calculadora Integradora Numérica Electrónica.
            *** Creada en 1946.
                **** En la universidad de Pensilvania. Diseñada por John Mauchly y J. Presper Eckert.
            *** Considerada como la primer computadora electrónica programable de propósito general.    
            *** Realizaba 5000 mil sumas y restas de 10 dígitos por segundo, mucho más que los demás dispositivos.
                **** Operó durante de 10, y se calcula que hizo más operaciones aritméticas que la humanidad en ese momento.
            *** Debido a los tubos del vacío, ENIAC solo operaba la mitad del día, hasta dañarse.
        **[#lightgreen] IBM 608
            *** Realizaba 4500 sumas.
            *** 80 divisiones o multiplicaciones por segundo.
            *** Esto acercaba las computadoras a las oficinas y los hogares.     
@endmindmap
```

## Arquitectura Von Neumann y Arquitectura Harvard
El siguiente mapa mental contiene la información de cada arquitectura para facilitar su comparación. También, una sección llamada "Lo básico", que fueron bases para la creación de ambas arquitecturas.

```plantuml
@startmindmap
<style>
    arrow{
        LineColor coral
    }
</style>
    *[#Orange] Arquitectura Von Neumann\n                      y\nArquitectura Harvard
        **[#lightblue] Lo básico
            *** Ley de Moore
                **** Establece que la velocidad del procesador o el poder de procesamiento\ntotal de las computadoras se duplica cada doce meses.
                    ***** Electrónica
                        ****** El número de transistores por chip se duplica cada año.
                        ****** El costo del chip permanece sin cambios.
                        ****** Cada 18 meses se duplica la potencia de cálculo sin modificar el costo.
                    ***** Performance
                        ****** Se incrementa la velocidad del procesador. Se incrementa la capacidad de la memoria.
                        ****** La velocidad de la memora corre siempre por detrás de la velocidad del procesador.
            *** Sistemas de cableado
                **** Sistemas cableados antiguas.
                    ***** Los datos se introducen, se pasan por una secuencia \nde funciones aritméticas y lógicas dando resultados.  
                        ****** Programación mediante Hardware
                            ******* Cuando se quiere realizar otra tarea se debe cambiar el hardware.
                **** Sistemas cableados actuales.
                    ***** La misma estructura que los sistemas antiguos, incluyendo ahora en\nla secuencia un intérprete de instrucciones que dan señales de control.
                        ****** Programación mediante software
                            ******* En cada paso se realiza alguna operación sobre los datos.  
        **[#lightblue] Arquitectura Von Neumann
            *** Modelo
                **** Es una arquitectura basada en la descrita en 1945\npor el matemático y físico John Von Neumann y otros,\nen el primer borrador de informe sobre el EDVAC.
                    ***** Los datos y programas se almacenan en una misma memoria de lectura-escritura.
                    ***** Los contenidos de esta memoria se acceden indicando su posición sin importar su tipo.
                    ***** Ejecución en secuencia (salvo que se indique lo contrario).
                    ***** Representación binaria.
            *** Contenido
                **** Este describe una arquitectura de diseño\npara un computador digital electrónico\ncon partes que constan de:
                    ***** Unidad central de procesamiento (CPU)
                        ****** Contiene una Unidad de control, una Unidad Aritmética Lógica u de Registros.
                    ***** Memoria principal
                        ****** Puede almacenar tanto instrucciones como datos.
                    ***** Sistema de Entrada/Salida.
                **** Con este modelo surge el concepto de programa almacenado, por el cuál se les \nconoce a las computadoras de este tipo también. La separación de la memoria\ny la CPU acarreó un problema denominado Neumann bottleneck.
                    ***** Esto se debe a que la cantidad de datos que pasa entre estos dos elementos difiere mucho\nen tiempo con las velocidades de ellos, por lo cual, la CPU puede permanecer ociosa.
                **** Interconexión Modelo Von Neumann
                    ***** Todos los componentes se comunican a través de un sistema de buses que se dividen en:
                        ****** Bus de datos, Bus de direcciones\ny bus de control.   
                **** Modelo de Bus
                    ***** El bus es un dispositivo en común entre dos o más dispositivos, si dos dispositivos transmiten\nal mismo tiempo señales están pueden distorsionarse y consecuentemente perder información.\nPor dicho motivo existe un arbitraje para decidir quién hace uso del bus.
                    ***** Cada línea puede transmitir señales que representan unos y ceros,en secuencia,\nde a una señal por unidad de tiempo.\nSi se desea transmitir 1 byte, se deberán mandar 8 señales, una detrás de otra,\nen consecuencia,se tardaría 8 unidades de tiempo.
                    ***** Existen varios tipos de buses que realizan la tarea de interconexión entre las distintas partes del\ncomputador, al bus que comunica al procesador, memoria y E/S se le denomina bus del sistema.
                    ***** Es un refinamiento del modelo Von Neumann.\nSu propósito es el de reducir la cantidad de conexiones entre la CPU y sus sistemas.
                    ***** La comunicación entre componentes se maneja por un camino compartido llamado Bus.
            *** Instrucciones
                **** La función de una computadora es la ejecución de programas.\nLos programas se encuentran localizados en memoria y consisten de instrucciones.
                **** La CPU es quien se encarga de ejecutar dichas instrucciones\na través de un ciclo denominado ciclo de instrucción.
                    ***** Las instrucciones consisten de secuencias de unos y ceros llamadas\ncódigo máquina y no son legibles para las personas.
                        ****** Por ello se emplean lenguajes como el \nensamblador o lenguajes de programación.
                ****  Las instrucciones son ejecutadas por la CPU a grandes velocidades.
            *** Ciclo de Ejecución
                **** UC obtiene la próxima instrucción de memoria\ny dejando la información en el\nregistro IR – Fetch de Instrucción.
                **** La instrucción es decodificada a un\nlenguaje que entiende la ALU – Decode de Instrucción.
                **** Obtiene de memoria los operandos requeridos por\nla instrucción – Calcular Operandos y Fetch de Operandos.
                **** La ALU ejecuta y deja los resultados en registros o \nen memoria – Execute Instrucción y White Operand.
        **[#lightblue] Arquitectura Harvard
            *** Modelo
                **** Originalmente refería a las Arquitecturas de Computadoras que\nutilizaban dispositivos de almacenamiento físicamente separados\npara las instrucciones y para los datos.
            *** Contenido
                **** Este describe una arquitectura de diseño\npara un computador digital electrónico con partes que constan de:
                    ***** Unidad central de procesamiento (CPU)
                        ****** La cual contiene una unidad de Control, una Unidad Aritmética Lógica y Registros.\nLa UC lee la instrucción de la memoria de instrucciones, genera las señales de control\npara obtener los operandos de memoria de datos y luego ejecuta la instrucción mediante \nla ALU almacenando el resultado en la memoria de datos.
                    ***** Memoria principal
                        ****** La cual puede almacenar tanto instrucciones como datos.
                            ******* Memoria de instrucciones
                                ******** Es la memoria donde se almacenan las instrucciones que debe ejecutar el microcontrolador.\nEl tamaño de las palabras de la memoria se adapta al número de bits de las instrucciones del\nmicrocontrolador. Se implementa utilizando memorias no volátiles ROM, PROM, EPROM, EEPROM o Flash.
                            ******* Memoria de datos
                                ******** Se almacenan los datos utilizados por los programas.\nLos datos varían continuamente y, por lo tanto, utiliza la memoria RAM,\nsobre la cual se pueden realizar operaciones de lectura y escritura.
                    ***** Sistema de Entrada/Salida.
            *** Memorias
                **** Fabricar memoria mucho más rápida tiene un precio muy alto. La solución,\npor tanto, es proporcionar una pequeña cantidad de memoria muy rápida\nconocida como Caché. Mientras los datos que necesita el procesador estén\nen la caché, el rendimiento será mucho mayor.
            *** Solución Harvard
                **** La arquitectura Harvard ofrece una solución particular a este problema.\nLas instrucciones y los datos se almacenan en cachés separadas para mejorar el rendimiento.\nPero, por otro lado, tiene el inconveniente de tener que dividir la cantidad de caché entre los dos,\npor lo que funciona mejor sólo cuando la frecuencia de lectura de instrucciones y de datos es \naproximadamente la misma.  
            *** Ejemplo de uso
                **** Esta arquitectura suele utilizarse en PICs,\no Microcontroladores, usados habitualmente en\nproductos de propósito especifico como electrodomésticos,\nprocesamiento de audio y vídeo, entre otras cosas.            
@endmindmap
```

## Basura Electrónica
Los electrónicos han ayudado a las tareas del ser humano, pero con el paso del tiempo, estos se quedan obsoletos o terminan su ciclo de vida. De esta forma se tiran a la basura sin tomar en cuenta que existen materiales que pueden ser reciclados y, que, a su vez, algunos de estos pueden ser dañinos para el medio ambiente.
```plantuml
@startmindmap
<style>
    arrow{
        LineColor lightcyan
    }
</style>
    *[#FFBBCC] Basura Electrónica
        **[#Orange] Oro en la basura\nlos secretos de\nla chatarra electrónica
            *** Uno de los elementos que se extrae de los terminales y de los procesadores es el oro.
                **** Casi todos los circuitos tienen metales preciosos como\noro, cobre, plata y cobalto.
            *** La mayoría de esta basura no es reciclada correctamente.\nEs exportada a países de tercer mundo\nque usan métodos peligrosos que son nefastos para el medio ambiente.\nEn estos lugares, los componentes son desembalados para extraer los metales preciosos.
                **** Se estima que 1 de cada 3 contenedores que sale de\nla Unión Europea contienen desechos electrónicos ilegales.
                **** Reciclar los componentes es un proceso costoso y complicado.
            *** Al no tener el equipo adecuado, derriten los componentes extrayendo el oro.\nMientras tanto inhalan los vapores tóxicos, luego botan los componentes quemados\ny estos terminan en el río contaminado el agua, que, a su vez, contamina todo.
                **** Los residuos quemados contaminan el agua y el aire,\ny muchos de los trabajadores terminan graves en salud.
            *** Los electrónicos también contienen datos
                **** Los discos duros, a pesar de estar dañados, estos pueden lograrse reparados y usarse para fines criminales.
            *** Algunos gobiernos ya están preocupándose por esto.
                **** La Unión Europea, implemento una ley en 2016 que requiere que cada país recoja\n45 toneladas por cada 100 toneladas de residuos electrónicos puestos a la venta.
            *** La basura electrónica sigue creciendo sin parar,\nesto gracias a la clase media “hambrienta” de nuevas tecnologías.
            *** Los productos electrónicos están diseñados para durar poco.\nObligando al consumidor a cambiarlos constantemente.
            *** Sin grandes cambios, esto podría considerarse como una gran plaga para el siglo XXI.

        **[#Orange] ¿Cómo se trata la\nchatarra electrónica?
            *** Los componentes se clasifican dependiendo de que tan ricos sean en minerales.\nUn especialista tiene que separarlos.
            *** Se hace una recolección, luego se clasifican para posteriormente\nser desarmados para recuperar materiales.
                **** De manera que puedan ser utilizados para la\ncreación de nuevos productos o exportados a otros continentes.
            *** Los residuos peligrosos deben ser manejados cuidadosamente,\nllevando otro método de reciclado.

        **[#Orange] Reciclaje de\ndesechos tóxicos
            *** Muchos de los materiales de los electrónicos pueden ser\nrecuperados en el proceso de reciclado para su uso en futuras aplicaciones.
            *** Estaño, hierro, silicio y una variedad de plásticos \nson los que se pueden encontrar en aparatos electrónicos.    
        **[#Orange] El reciclaje de materiales electrónicos
            *** Primero se desmantela el componente y las carcasas se limpian.\nLos cables se separan en plástico y cobre.
                **** Los plásticos se separan, las terminales se separan,\nla chatarra y componentes se separan.\nEl aluminio es otra clasificación.
            *** De las tarjetas madres se separan por chips, basura, cobre y bronce,\n los tornillos.
                **** Estos se almacenan para que algún cliente del exterior lo compre.
            *** Todo se recicla y se reutiliza, algunos materiales\nson triturados para poder ser materia prima.
                **** Subsidia mas costos al crear materias primas, plásticos,\nvidrios, materiales ferrosos y no ferrosos y electrónica.
        **[#Orange] Empresas Recicladoras
            *** E-END Estados Unidos
            *** Perú Green Recycling – Perú
            *** Techemet – México
            *** Remsa – México 
            *** Servicios Ecológicos – Costa Rica
@endmindmap
```